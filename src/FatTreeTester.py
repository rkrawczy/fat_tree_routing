import fattree
import argparse

#Function used to demonstrate how to use the methods as an API
#Covers all of the possible typical appliation use cases (see README.txt)
def param_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument('-nit', default='i', help='program input file type: i(bnetdiscover) s(imple)')

    parser.add_argument('-ni', help='path to program network input file', required=True)

    parser.add_argument('-v', help='be verbose', action='store_true')

    parser.add_argument('-dsf', help='use more verbose, debugging, non-OpenSM-compliant shift file format', action='store_true')

    parser.add_argument('-no', help='optional output file directory with simple network format')

    parser.add_argument('-r', help='do routing', action='store_true')

    parser.add_argument('-t', help='do network test', action='store_true')

    parser.add_argument('-lo', help='optional output file with lfts for OpenSM')

    parser.add_argument('-gl', help='optional guid2lid OpenSM input file with guid2lid for GUID-LID assignment')

    parser.add_argument('-lfts', help='optional input lfts from external file (done instead of routing)')

    parser.add_argument('-so', help='optional path to read input shift order file for network test')

    parser.add_argument('-sf', help='optional path to produce shift order output file')

    parser.add_argument('-rl', help='optional path to input root switches list file for switching')

    args = parser.parse_args()


## READ ROOTS LIST
    if args.rl  != None:
      root_switches = fattree.IO.read_roots_from_file(args.rl)

    if args.nit == 'i':
        network = fattree.IO.read_network_from_ibnetdiscover_file(
            args.ni)
    elif args.nit == 's':
        network = fattree.IO.read_network_from_simple_file(
            args.ni)
    else:
        print("UNKNOWN input file format")
        return


    if args.gl:
        #
        fattree.FatTreeAlg.get_network_LIDs_from_file(network, args.gl)
    else:
        #Allow assigning the LIDs in the application
        fattree.FatTreeAlg.set_network_LIDs(network)
    if args.v :
        print("ParamParser: Printing out the used network topology")
        fattree.NetworkChecker.print_network(network)
    if args.no  != None:
        print("ParamParser: Writing simple network file")
        fattree.IO.write_to_simple_file_from_network(
            args.no,
            network)
    if args.r:
        if args.rl == None:
          print("ERROR. ROOT switches list required when doing routing")
          return
        routing_tables,network_metadata = fattree.FatTreeAlg.generate_lfts_from_routing(
            network,root_switches,args.v)
    elif args.lfts:
        if args.rl == None :
          print("ERROR. ROOT switches list required when doing reading the LFTs")
          return
        if args.gl == None :
          print("ERROR. GUID to LID mapping required when reading the LFTs")
          return
        routing_tables,network_metadata = fattree.FatTreeAlg.get_lfts_from_file(
            network,
            args.lfts,
            root_switches,
            args.v
            )
    else:
        print("INFO - Neither routing nor LFTs reading selected. Closing.")
        return
    if args.v:
        print("\nParamParser: Printing the output LFTs of the routing algorithm\n")
        fattree.FatTreeAlg.print_routing_tables(routing_tables, True)
    if args.lo:
        fattree.IO.write_lfts_opensm_compliant_file(args.lo, routing_tables)
    if args.so:
        shift_order = fattree.FatTreeAlg.get_shift_pattern_from_file(args.so, network_metadata)
    else:
        shift_order = fattree.FatTreeAlg.generate_shift_pattern_from_network(network_metadata)
    if args.sf:
        fattree.IO.write_opensm_shift_file(args.sf, shift_order, args.dsf)
    if args.t:
        if shift_order == None:
            print("ERROR - NO SHIFT PATTERN")
            return
        conflicts = fattree.NetworkTrafficTester.check_linear_shift(network,routing_tables,
            shift_order, args.v)
        if(conflicts == []):
          print("\nParamParser: Summary for all possible shift distances: NO CONFLICTS")
        else:
          print("\nParamParser: Summary for all possible shift distances: CONFLICTS OCCURRED")


if __name__ == '__main__':
    param_parser()
