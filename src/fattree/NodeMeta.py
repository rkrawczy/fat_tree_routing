
from fattree.Node import *
from fattree.PortMeta import *

#Class to hold Node Metadata
#Used mostly to hold additional information for the routing algorithm
class NodeMeta:
    def __init__(self, node, isSpine=False,level=-1):
        self.portsMetaList=[]
        self.node=node
        #flag to check if it's the root (=spine, for our case) switch
        self.isSpine=isSpine
        #Node level, 0 for root, 1 for leaf, 2 for an HCAs
        self.level=level
        #node name / type
        self.type=type(node).__name__
        #place keepers for the node
        #Used in switches to have dummies
        #Used in the switch to have correct shift pattern
        #For switches having less than maximal number of ports populated
        self.placeKeepers=[]
        for port in node.ports:
            self.portsMetaList.append(PortMeta(port))

    # Find metadata for the given port
    def find_meta_by_port(self, port):
        for port_meta in self.portsMetaList:
            if port_meta.port == port:
                return port_meta

    @staticmethod
    def find_node_meta_by_lid(lid, allMeta):
        for meta_node in allMeta:
            if meta_node.type == "Switch":
                if meta_node.node.nodeLid == lid:
                   return meta_node, meta_node.node.nodeGuid
            if meta_node.type == "Hca":
                port = meta_node.node.get_port_by_lid(lid)
                if  port != []:
                    return meta_node, port.portGuid
        #NODE NOT FOUND
        return -1,-1

    @staticmethod
    def find_node_meta_by_guid(guid, allMeta):
        for meta_node in allMeta:
            if meta_node.type == "Switch":
                if meta_node.node.nodeGuid == guid:
                   return meta_node
            if meta_node.type == "Hca":
                node = meta_node.node
                port = node.get_port_by_guid(guid)
                if  port != []:
                    return meta_node
        #NODE NOT FOUND
        return -1
