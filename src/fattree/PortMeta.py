from fattree.Port import *

# Class to hold the metadata on the port
# Used for the Routing algorithm
# It holds information on how many times it was used as a down or up link
# It also holds information on the level of the port
# Meaning the depth in the fat tree (root/spine switches have 0, leaves have 1)
# It is used mostly for routing to uniformly distribute the load in the LUTs
class PortMeta():
    def __init__(self, port):
        # in this configuration the "down link" is used only once
        self.usedForDown=0
        #For the up links we need a counter
        self.usedForUp = 0
        self.port=port
        self.neighborLevel=-1

    #TODO
    #def get_neighbor_level(self, PortsMeta):
    #    pass
    #TODO
    #@staticmethod
    #def get_meta_by_guid(self, PortsMeta):
    #    pass

    #Get port metadata knowing the switch metadata and the port number
    @staticmethod
    def get_meta_by_port_number(portNumber,switchMeta):
        for port_meta in switchMeta.portsMetaList:
            if port_meta.port.portNo==portNumber:
                return port_meta
        return -1