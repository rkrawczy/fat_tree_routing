from fattree.Node import *
from fattree.NodeMeta import *
from fattree.FatTreeAlg import *
from fattree.TrafficEntry import *
from fattree.ConflictEntry import *

from fattree.FatTreeAlg import FatTreeAlg

# Class to test if there are no conflicts in the linear shift pattern
class NetworkTrafficTester:

    # Simulate linear shift given the hosts pattern,
    # the routing tables and the network with its metadata
    # The methods will test all possible shift distances of the nodes and will
    # Record conflict occurrences (if any)
    # Method returns conflicts - full list of objects of ConflictEntry class
    # For all of the shift distances
    @staticmethod
    def check_linear_shift(network, routingTables, shiftPattern, beVerbose=False):
        #create network metadata array to track usage
        conflicts = []

        hcas_n_keepers=shiftPattern
        for shift_distance in range(1, len(hcas_n_keepers)):
            #print("TESTING FOR LINEAR SHIFT: ", shift_distance)
            network_usage_list = []
            shift_conflict=False
            for idx,shift_node in enumerate(hcas_n_keepers):
                if type(shift_node).__name__ == "PlaceKeeper"  or  shift_node == "DUMMY":
                    continue
                shift_node_index = idx
                neighbor_index = (shift_node_index+shift_distance)%len(hcas_n_keepers)
                neighbor=hcas_n_keepers[neighbor_index]
                if type(neighbor).__name__ == "PlaceKeeper" or  neighbor== "DUMMY":
                    continue
                #Now for every node or HCAs simulate the transfer of a packet
                #with marking what links and ports were used
                #Thereby keeping record on the network usage for conflict detection
                local_conflicts = NetworkTrafficTester.__simulate_send_to_hca(shift_node, neighbor,
                                                                              network, routingTables,
                                                                              network_usage_list)
                if local_conflicts!=[]:
                    for conflict in local_conflicts:
                        conflict.shift_distance=shift_distance
                        conflicts.append(conflict)
                    shift_conflict=True

            if beVerbose:
              if shift_conflict==False:
                print("NO CONFLICT FOR SHIFT DISTANCE: ", shift_distance)
              else:
                print("CONFLICT OCCURRENCE FOR SHIFT DISTANCE: ", shift_distance)

        return conflicts

    #Simulate a packet being sent from source HCA to destination HCA
    #Made in order to mark what ports were used for this transmission
    #This is to track the ports usage for a given shift distance
    # in order to detect the conflicts for a given linear shift distance
    # Input params: src/ dest HCAs,
    # network, routing tables with LFTs,
    # list to track network (ports) usage
    # Output params: conflicts list that occurred for this single send
    @staticmethod
    def __simulate_send_to_hca(sourceNode, destNode, network, routingTables, networkUsageList):
        conflicts=[]
        if type(sourceNode).__name__ == "Hca":
            source_GUID = sourceNode.ports[0].portGuid
            current_switch=FatTreeAlg.find_node_by_GUID(sourceNode.ports[0].neighborGuid, network)
        else:
            source_GUID = sourceNode.dummyGuid
            current_switch=FatTreeAlg.find_node_by_GUID(sourceNode.parentGuid, network)
        if type(destNode).__name__ == "Hca":
            dest_GUID = destNode.ports[0].portGuid
        else:
            dest_GUID = destNode.dummyGuid
        #simulate routing of data to destination
        #and keep track on used ports on switches
        #Look through the hopped nodes until reaching the destination
        while True:
            current_switch_lft_entry = RoutingTable.find_lft_dest_entry(routingTables, current_switch.nodeGuid, dest_GUID)

            hop_remote_GUID=current_switch_lft_entry.nextHopGuid
            if(hop_remote_GUID==dest_GUID):
                break
            else:
                #create port usage traffic entries based ont the routing LFTs data
                hop_port_no_current_switch=current_switch_lft_entry.localHopPortNo
                hop_port_no_remote_switch=current_switch_lft_entry.remoteHopPortNo

                local_entry=TrafficEntry(sourceGuid=source_GUID, destinationGuid=dest_GUID,
                                         switchGuid=current_switch.nodeGuid, switchPortNo=hop_port_no_current_switch,
                                         function=Function.SENDING)
                remote_entry=TrafficEntry(sourceGuid=source_GUID,destinationGuid=dest_GUID,
                                     switchGuid=hop_remote_GUID,switchPortNo=hop_port_no_remote_switch,
                                          function=Function.RECEIVING)

                #lid_current_switch = local_entry.NodeLID
                #lid_remote_switch = remote_entry.NodeLID
                #lid_current_switch = "NA"
                #lid_remote_switch = "NA"

                local_used, coflicting_entry_l = TrafficEntry.check_if_port_used(local_entry.switchGuid,
                                                                                 local_entry.switchPortNo, networkUsageList,
                                                                                 Function.SENDING)
                remote_used, coflicting_entry_r = TrafficEntry.check_if_port_used(remote_entry.switchGuid,
                                                                                  remote_entry.switchPortNo, networkUsageList,
                                                                                  Function.RECEIVING)

                current_switch=FatTreeAlg.find_node_by_GUID(hop_remote_GUID, network)
                #check if the entries aren't already in the taken ports.
                if local_used==True:
                    conflicting_routes=[]
                    conflicting_routes.append((local_entry.sourceGuid, local_entry.destinationGuid))
                    conflicting_routes.append((coflicting_entry_l.sourceGuid, coflicting_entry_l.destinationGuid))
                    conflict = ConflictEntry(node=local_entry, nodeGuid=local_entry.switchGuid, nodePortNo=local_entry.switchPortNo, conflictingRoutes=conflicting_routes)
                    conflicts.append(conflict)

                networkUsageList.append(local_entry)

                if remote_used==True:
                    conflicting_routes=[]
                    conflicting_routes.append((remote_entry.sourceGuid, remote_entry.destinationGuid))
                    conflicting_routes.append((coflicting_entry_r.sourceGuid, coflicting_entry_r.destinationGuid))
                    conflict = ConflictEntry(node=remote_entry, nodeGuid=remote_entry.switchGuid, nodePortNo=remote_entry.switchPortNo, conflictingRoutes=conflicting_routes)
                    conflicts.append(conflict)

                networkUsageList.append(remote_entry)

        return conflicts
