from fattree.Node import *

# Class to represent  Switch Node
# The whole switch has its own GUID shared by all of its ports
class Switch(Node):
    def __init__(self, nodeGuid, ports=[], maxPorts=40):
        Node.__init__(self, ports, maxPorts)
        self.nodeGuid=nodeGuid
        self.nodeLid=-1