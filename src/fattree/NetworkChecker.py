## Class used to display the network
## Can be used check if the existing network is coherent with the actual network
class NetworkChecker:
    @staticmethod
    def print_network(network):
        for node in network:
            print("Node: " + type(node).__name__ + " with " + str(node.max_ports) + " ports:")
            for index, port in enumerate(node.ports):
                print("Port " + str(port.portNo) + " GUID" + str(port.portGuid) + " LID " + str(port.LID) + " Neighbor Port Number " + str(port.neighborPortNo) + " Neighbor GUID " + str(port.neighborGuid))
            print("\n")
        print("\n\n")