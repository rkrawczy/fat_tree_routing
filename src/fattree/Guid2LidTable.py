# Class to hold LID to GID mapping for the node
# Used to read the LID mapping from the OpenSM guid2lid output file
# Which can be found in the cache directory: by default /var/cache/opensm/guid2lid
class Guid2LidTable:
    def __init__(self):
        self.__table=[]

    #Add new entry to the table
    def add_entry(self, guid, lid):
        self.__table.append([guid,lid])

    #Get GUID given the LID
    def get_guid_by_lid(self, lid):
        for entry in self.__table:
            if entry[1]==lid:
                print("guid2lidtable found entry")
                return entry[0]
        return -1

    #Get LID given the GUID
    def get_lid_by_guid(self, guid):
        for entry in self.__table:
            if entry[0]==guid:
                return entry[1]
        return -1

    #Get the Guid and LID table entries
    def get_all_entries(self):
        return self.__table
