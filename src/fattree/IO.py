import pickle
#import gnosis.xml.pickle
#import gnosis.xml.pickel
#from dict2xml import dict2xml
#import json
#import jsonpickle
from fattree.Hca import *
from fattree.Node import *
from fattree.Switch import *
from fattree.Guid2LidTable import *
from fattree.RoutingTable import *

import re

# Class with methods that handle file and i/O operations
# Contains readers from OpenSM and IB applications
# Contains producers of OpenSM Compliant Data
class IO():

    # Produce simple network topology file
    @staticmethod
    def write_to_simple_file_from_network(outFile, network):
        text_file = open(outFile, "w")

        #text_file.write("\n\n")

        #Put all HCAs first
        for node in network:
            if type(node).__name__=="Hca":
                text_file.write(
                    "NODE->HCA \n")
                for port in node.ports:
                    text_file.write(
                        "  PORT_GUID: "
                        + hex(port.portGuid)
                        + " PORT_NO: "
                        + str(port.portNo)
                        + " NEIGHBOUR_GUID: "
                        + hex(port.neighborGuid)
                        + " NEIGHBOR_PORT_NO: "
                        + str(port.neighborPortNo)
                        + "\n")

        text_file.write("\n\n")
        #Now service switches
        for node in network:
            if type(node).__name__=="Switch":
                text_file.write("NODE->SWITCH GUID: " + hex(node.nodeGuid) + "\n")
                for port in node.ports:
                    text_file.write(
                        "  PORT_NO: "
                        + str(port.portNo)
                        + " NEIGHBOR_GUID: "
                        + hex(port.neighborGuid)
                        + " NEIGHBOR_PORT_NO: "
                        + str(port.neighborPortNo)
                        + "\n")
            pass
        pass
        text_file.close()

    '''
        @staticmethod
        def write_binary_to_file(setup,outFile):
            pickle.dump(setup, outFile, protocol=None)

        @staticmethod
        def read_binary_from_file(inFile):
            input = pickle.load(inFile)
            return input
    '''

    # def write_xml_to_file(setup,outFile):
    #    pickle.dump(setup, outFile)

    # @staticmethod
    # def read_xml_from_file(inFile):
    #    input = pickle.load(inFile)
    #    return input
    '''
        @staticmethod
        def read_json_from_file(inFile):
            jsonpickle.set_encoder_options('simplejson', indent=2)
            text_file = open(inFile, "r")
            output = text_file.read()
            text_file.close()
            output_json = jsonpickle.decode(output)
            return output_json

        def write_json_to_file(setup,outFile):
            jsonpickle.set_encoder_options('simplejson', indent=2)
            json_string = jsonpickle.encode(setup)
            print("JSON STRING:")
            print(json_string)
            #result = jsonpickle.decode(json_string)

            text_file = open(outFile, "w")
            text_file.write(json_string)
            text_file.close()
            #json_string = jsonpickle.encode(setup)
            #my_json_object = json.dumps(setup)
            #print(my_json_object)
            #json.dump(setup, outFile)
    '''

    # Read network topology from the in-application-defined simple network topology file
    @staticmethod
    def read_network_from_simple_file(inFile):
        network = []
        text_file = open(inFile, "r")
        raw_text = text_file.read()

        node_divided_text = raw_text.split("NODE->")

        #Do all the HCAs
        for entry in node_divided_text:
            if entry.find("HCA") != -1:
                line_split_entry=entry.split("\n")
                ports_entries = []
                for line_entry in line_split_entry:
                    if line_entry.find("PORT_GUID") != -1:
                        values = line_entry.split(" ")
                        port_guid = int(values[3], 16)
                        port_no = int(values[5])
                        n_guid = int(values[7], 16)
                        n_port_no = int(values[9])

                        ports_entries.append(
                            Port(portNo=port_no, portGuid=port_guid,
                                neighborGuid=n_guid, neighborPortNo=n_port_no))

                network.append(Hca(ports=ports_entries, maxPorts=len(ports_entries)))

        #Now do all the Switches
        for entry in node_divided_text:
            if entry.find("SWITCH") != -1:
                line_split_entry=entry.split("\n")
                switch_info_line = line_split_entry[0].split(" ")
                switch_guid = int(switch_info_line[2], 16)

                ports_entries = []
                for line_entry in line_split_entry:

                    if line_entry.find("PORT_NO") != -1:
                        values = line_entry.split(" ")
                        port_no = int(values[3])
                        n_guid = int(values[5], 16)
                        n_port_no = int(values[7])

                        ports_entries.append(
                            Port(portNo=port_no, portGuid=switch_guid,
                                 neighborGuid=n_guid, neighborPortNo=n_port_no))

                network.append(Switch(
                    nodeGuid = switch_guid,
                    ports=ports_entries,
                    maxPorts=len(ports_entries)))

        text_file.close()
        return network

    #Read the list of root switches from the file
    def read_roots_from_file(inFile):
        text_file = open(inFile, "r")
        raw_text = text_file.read()
        text_file.close()
        roots_array = raw_text.split("\n")
        roots_array.pop()
        roots=[]
        for root_entry in roots_array:
            roots.append(int(root_entry, 16))
        return roots

    # Get network topology from the output produced by the ibnetdiscover file
    def read_network_from_ibnetdiscover_file(inFile):
        text_file = open(inFile, "r")
        raw_text = text_file.read()
        text_file.close()
        network=[]

        nodes_array = raw_text.split("vendid")
        del(nodes_array[0])

        for node_entry in nodes_array:
            node_line_split=node_entry.split("\n")
            type_info=node_line_split[4].split()
            if(type_info[0]=='Switch'):
                guid_full=node_line_split[3].replace("switchguid=","")
                switch_guid = re.sub('.*\(','',guid_full)
                switch_guid = re.sub('\)', '', switch_guid)
                switch_guid_int=int(switch_guid, 16)

                #GUID done, now ports
                ports_entries = []
                port_lines=[n for n in node_line_split if len(n)!=0 and n[0]=="["]
                for port_line in port_lines:
                    #print(port_line)
                    ports = re.search(r"\[(.*)\].*\[(.*)\]",port_line)
                    host_port_no = int(ports.group(1))


                    remote_port_no = int(ports.group(2))
                    remote_guid=re.search(r"\((.*)\)",port_line)

                    if remote_guid==None:
                        #NO GUID FOUND. ASSUMING SWITCH. GETTING FROM NAME INSTEAD
                        remote_guid = re.search(r"\"S-(.*?)\"", port_line)

                    remote_guid_int=int(remote_guid.group(1), 16)

                    if host_port_no==41:
                        pass
                        #PORT 41 in switch used which is a virtual port. INGORING"
                    else:
                        ports_entries.append(
                            Port(portNo=host_port_no, portGuid=switch_guid_int,
                                neighborGuid=remote_guid_int, neighborPortNo=remote_port_no))

                network.append(Switch(nodeGuid=switch_guid_int, ports=ports_entries, maxPorts=40))
            elif(type_info[0]=="Ca"):
                if node_line_split[4].find("Mellanox Technologies Aggregation Node") != -1:
                    # Ommit some wicked Ca alias that Mellanox uses for their switch nodes
                    pass
                else:
                    host_name = type_info[4][1:]
                    port_name = type_info[5][0:-1]
                    ports_entries = []
                    port_lines = [n for n in node_line_split if len(n) != 0 and n[0] == "["]
                    for port_line in port_lines:
                        #print(port_line)
                        ports = re.search(r"(\[.*\]).*(\[.*\])", port_line)
                        host_port_no_str = re.findall('[0-9]+', (ports.group(1)))
                        remote_port_no_str = re.findall('[0-9]+',(ports.group(2)))
                        host_port_no = int(host_port_no_str[0])
                        remote_port_no = int(remote_port_no_str[0])
                        local_guid = re.search(r"\((.*)\)", port_line)
                        local_guid_int = int(local_guid.group(1), 16)
                        remote_guid_whole=re.search('S-(.*)\"\[', port_line)
                        remote_guid=remote_guid_whole.group(1)
                        remote_guid_int=int(remote_guid,16)

                        ports_entries.append(
                            Port(portNo=host_port_no, portGuid=local_guid_int,
                                 neighborGuid=remote_guid_int, neighborPortNo=remote_port_no))

                    network.append(Hca(ports=ports_entries, maxPorts=1,
                                       portName=port_name, hostName=host_name))
        return network

    # Get GUID 2 LID mapping from the guid2lid file
    @staticmethod
    def read_guid_2_lid_table_from_guid2lid(inFile):

        guid_2_lid_table = Guid2LidTable()

        lid_file_desc=open(inFile, "r")
        raw_lids_text = lid_file_desc.read()
        lid_file_desc.close()
        lids_split = raw_lids_text.split("\n\n")
        lids_split.pop()
        for entry in lids_split:
            guid_lid = entry.split(" ")
            guid_2_lid_table.add_entry(int(guid_lid[0],0), int(guid_lid[1],0))

        return guid_2_lid_table

    # Read LFTs from the file
    # Needs metadata update before using
    # Call update_metadata_from_file_read_lfts from FatTreeAlg
    # after invoking to update the metadata
    @staticmethod
    def read_tables_from_LFT_file(inFile):

        lft_file_desc=open(inFile, "r")
        raw_lfts_text = lft_file_desc.read()
        lft_file_desc.close()

        routing_tables = []
        switches_split = raw_lfts_text.split("Unicast")
        switches_split.pop(0)

        for switch_entry in switches_split:
            all_switch_entry_lines = switch_entry.split("\n")
            first_line = all_switch_entry_lines[0]
            first_line_split = first_line.split(" ")

            switch_guid = int(first_line_split[8],0)
            #switch_lid = int(first_line_split[6],0)


            #clean the list out of all that is not an lft entry
            all_switch_entry_lines.pop(0)
            all_switch_entry_lines.pop()
            all_switch_entry_lines.pop()

            rtable = RoutingTable(switch_guid)
            for ltf_entry in all_switch_entry_lines:
                index_hca = ltf_entry.find("HCA")
                index_sw = ltf_entry.find("switch")
                lft_entry_split = ltf_entry.split(" ")

                lft_destination_LID = int(lft_entry_split[0],0)
                lft_local_hop_port_no = int(lft_entry_split[1])
                #local port 0 means the local node in which we are now
                #ignore that entry
                if lft_local_hop_port_no == 0:
                    continue

                if index_sw != -1:
                    lft_type="Switch"
                elif index_hca != -1:
                    lft_type = "Hca"
                else:
                    lft_type="Unknown"

                lft_entry = RoutingTableEntry(
                    destinationGuid=-1,
                    destinationLid=lft_destination_LID,
                    destinationType=lft_type,
                    nextHopGuid=-1,
                    nextHopLid=-1,
                    localHopPort=-1,
                    #remote_hop_port=-1, #TODO
                    localHopPortNo=lft_local_hop_port_no,
                    remoteHopPortNo=-1,
                    isForPlaceKeeper=False)
                rtable.routingTableEntries.append(lft_entry)
            routing_tables.append(rtable)
        return routing_tables

    #Read the linear shift nodes pattern
    #From the OpenSM-compliant opensm-ftree-ca-order.dump file
    @staticmethod
    def read_shift_pattern_order_from_file(inFile):
        pattern_order=[]
        pattern_file_desc=open(inFile, "r")
        pattern_text = pattern_file_desc.read()

        pattern_file_desc.close()

        pattern_text_split = pattern_text.split("\n")
        pattern_text_split.pop()

        for node in pattern_text_split:
            node_split = node.split("\t")
            lid=node_split[0]
            pattern_order.append(int(lid,0))

        return pattern_order

    #Write LFTs into a file
    #That is compliant with the OpenSM opensm-lfts.dump file format
    @staticmethod
    def write_lfts_opensm_compliant_file(outFile, routingTables):
        file_desc=open(outFile, "w")

        for routingTable in routingTables:
            file_desc.write("Unicast lids guid " + hex(routingTable.switchGuid) + "  \n")
            for lftEntry in routingTable.routingTableEntries:
                #Format
                #<LID> <PORT_NO> : portguid <dest_port_guid_in_hex>
                #0x0001 015: (Channel Adapter portguid 0x7cfe9003005845f
                file_desc.write(hex(lftEntry.destinationLid) + "  ")

                file_desc.write(str(lftEntry.localHopPortNo) + " ")
                file_desc.write(" portguid ")
                #port GUID number has to be incremented
                #this is because of the OpenSM quirk to distinct for
                #HCA GUID and its ports GUIDs
                #according to formula HCA_GUID + port_no

                if lftEntry.destinationType == "Hca":
                    file_desc.write(hex(lftEntry.destinationGuid + 1))
                else:
                    file_desc.write(hex(lftEntry.destinationGuid))

                file_desc.write(" \n")
        file_desc.close()

    #Write LFTs into a file
    #That is compliant with the OpenSM opensm-lfts.dump file format
    # osf_compliance set to false makes the output less verbose & OpenSM-compliant
    # otherwise the shift pattern contains also GUID
    @staticmethod
    def write_opensm_shift_file(outFile, shiftArray, debuggingInfo=False):

        file_desc=open(outFile, "w")

        if debuggingInfo == True:  # Add additional info tha tis not on the OpenSM output
          file_desc.write("LID" + "\t" + "GUID" + "\t" + "HOST_NAME" + " " + "PORT_NAME\n"  )

        for shift_entry in shiftArray:
            if shift_entry == "DUMMY":
                file_desc.write(str("0xFFFF") + "\t" + str("DUMMY"))
                file_desc.write(" \n")
            else:
                node_name=shift_entry.hostName
                port_name=shift_entry.portName
                LID_string = "{0:#0{1}x}".format(shift_entry.ports[0].portLid, 6)
                if debuggingInfo == False: # write only what is in the OpenSM
                  file_desc.write(LID_string + "\t" + node_name + " " + port_name)
                else: # write all relevant info for the shift
                    file_desc.write(LID_string + "\t" + hex(shift_entry.ports[0].portGuid) + "\t" + node_name + " " + port_name)
                file_desc.write(" \n")

        file_desc.close()
'''
    #TODO - make not from ibnodes but from ibnetdiscover !
    def get_hosts_array_from_ibnodes_file(inFile):
        file_desc=open(inFile, "r")
        pattern_text = file_desc.read()
        file_desc.close()

        pattern_text_lines=pattern_text.split("\n")
        hosts = []

        for line in pattern_text_lines:
            line_array=line.split(" ")
            if line_array[0].find("Ca")!= -1:
                print("Found Ca")

                hostname = line_array[4]
                hostname=hostname[1:]
                if hostname.find("Quantum Mellanox Technologies")!=-1:
                    print("FOUND AGGREGATION NODE. LEAVING")
                    continue
                GUID = int(line_array[1],16)
                ib_no_string = line_array[5]
                ib_no_string = ib_no_string[:-1]
                if ib_no_string=="HCA-1" or ib_no_string=="mlx5_0":
                    ib_int_no=0
                elif ib_no_string=="HCA-2" or ib_no_string=="mlx5_1":
                    ib_int_no=1
                else:
                    print("HOST NUMBER UNKNOWN")
                    ib_int_no = -1
                hosts.append((hostname, ib_int_no, GUID))
            pass

        return hosts
'''