from fattree.Port import *
from fattree.PortMeta import *

#Class to represent the PORT GROUP metadata
# Group group is used in the fat tree routing algorithm
# It corresponds to the ports that connect the same pair of switches
# For our use case there are 2 ports per group
class PortMetaGroup():
    def __init__(self, guid):
        self.guid=guid
        self.portsMetas=[]

    #Check if all of the neighbors of this group have the same level
    #Regural fat tree is needed so all group neighbors should have the same level
    # If not that this is a critical error
    def get_n_check_ports_level(self):
        ref_level = self.portsMetas[0].neighborLevel

        for meta in self.portsMetas:
            if meta.neighborLevel != ref_level :
                print("FATAL. DIFFERENT LEVELS OF LINKS IN THE GROUP. ABORTING")
                exit()

        return ref_level

    #Retreive the group given the GUID to which they are connected to
    #Used to retreive group of ports identified by the GUID of switch
    #to which all of the group ports they are connected to
    @staticmethod
    def find_by_guid(portMetaGroupArray, guid):
        for group in portMetaGroupArray:
            if group.guid == guid:
                return group
        return []


    #Retreive the least used port in the group
    #Methos used for even load distribution to the links in the fat tree
    @staticmethod
    def find_least_loaded_port_meta(portMetaGroup):
        min_load_port_meta=portMetaGroup.portsMetas[0]
        for port_meta in portMetaGroup.portsMetas:
            if port_meta.usedForUp<min_load_port_meta.usedForUp:
                min_load_port_meta=port_meta
        return min_load_port_meta

    #if empty
