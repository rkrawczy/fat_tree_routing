# Class to hold an entry of a Conflict when routing
#Contains precise info on the shared conflicting port
# And a collection of Conflicting routes - a pairs of source-destination GUIDs
class ConflictEntry:
    def __init__(self, shiftDistance=-1, node=None, nodeGuid=0, nodePortNo=-1, conflictingRoutes=[]):
        self.shift_distance=shiftDistance

        self.node=node
        self.nodeGuid=nodeGuid
        self.nodePortNo=nodePortNo

        #LIst of GUID pairs of SRC and DST that were co-using this port
        self.conflictingRoutes=conflictingRoutes
