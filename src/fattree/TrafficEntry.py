from enum import Enum

class Function(Enum):
    SENDING=0
    RECEIVING=1

# Class to hold the
# These entries are generated for testing a particular linear shift scheme
# To record if the particular port has been already used
# In principle, if there is no conflict than no port should be reused
class TrafficEntry:

    def __init__(self, sourceGuid, destinationGuid,
                 switchGuid, switchPortNo, function):
        self.sourceGuid=sourceGuid
        self.destination_GUID=destinationGuid
        self.switchGuid=switchGuid
        self.switchPortNo=switchPortNo
        #function for sending/receiving
        #added to differentiate for the links support duplex
        #and typically links use both of the functions
        self.function=function

    #check if a given port is used in this particular function (up/down)
    @staticmethod
    def check_if_port_used(guid, portNo, networkUsageList, function):
        for entry in networkUsageList:
            if entry.switchGuid==guid and entry.switchPortNo==portNo and entry.function==function:
                return True, entry
        return False, None
