from fattree.RoutingTableEntry import *

# Class used to hold the LFT entries for a given switch
# Contrins GUID of the switch and the LFTs
class RoutingTable():
    def __init__(self, switchGuid):
        self.switchGuid=switchGuid
        self.routingTableEntries=[]

    #Return the Routing table for the switch identified by the GUID
    @staticmethod
    def find_switch_lft_by_guid(routingTables, guid):
        for switch_table in routingTables:
            if switch_table.switchGuid==guid:
                return switch_table

    #Find LFT lookup entry for a given switch defining a routing to a given destination GUID
    def find_lft_dest_entry(routingTables, guid, destGuid):
        lft =  RoutingTable.find_switch_lft_by_guid(routingTables, guid)
        for lft_entry in lft.routingTableEntries:
            if lft_entry.destinationGuid==destGuid:
                return lft_entry
        #NO LFT ENTRY FOUND
        return []
