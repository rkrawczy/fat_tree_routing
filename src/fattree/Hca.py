from fattree.Node import *

# Class to represent an IB HCA
class Hca(Node):
    def __init__(self, ports = [], maxPorts = 1, portName ="", hostName =""):
        Node.__init__(self, ports, maxPorts)
        #2 HCA-related strings with useful HCAs info in the shift pattern file
        #Should be rather port-related, but ibnetdiscover keeps this info
        #for the Ca instead and then lists ports
        #This is another SW Mellanox stack oddity
        self.portName = portName
        self.hostName = hostName

    #Add 1 just as opensm does it (Mellanox Bug for some of its stack versions)
    def transform_port_guid_topo_to_opensm(self, portNo):
        port = self.get_port_by_num(portNo)
        return self.transform_guid_topo_to_opensm(port.portGuid)

    #Add 1 just as opensm does it (Mellanox Bug for some of its stack versions)
    def transform_guid_topo_to_opensm(self, guid):
        return guid + 1