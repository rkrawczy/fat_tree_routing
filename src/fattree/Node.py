from fattree.Port import *

#Class to represent a node in the network
#Used as a base class for an HCA, a PlaceKeeper and a Switch
class Node:
    #List of ports of this node. Typically 1 for HCA and 40 for a Switch
    #List of objects of "Port" class
    #Keeps information only on the POPULATED ports !
    #Note- index in the list is NOT a port number
    #Class "Port" has a separate field for the physical number
    ports=[]
    #Constructor assigning ports to the node
    def __init__(self, ports = [], maxPorts = 1):
        self.ports=ports
        self.max_ports=maxPorts

    #Get port given its GUID
    #Used typically for the HCAs
    #Where multiple GUIDs can occur, one per port
    def get_port_by_guid(self, guid):
        for port in self.ports:
            if port.portGuid==guid:
                return port
        return []

    #Get port given its GUID
    #Used typically for the HCAs mostly
    #Where multiple LIDs can occur, one per port
    def get_port_by_lid(self, LID):
        for port in self.ports:
            if port.portLid==LID:
                return port
        return []

    #Get the port object given the physical number
    #Used typically to retreive the ports in the switches
    def get_port_by_num(self, PortNo):
        for port in self.ports:
            if port.portNo==PortNo:
                return port
        return []