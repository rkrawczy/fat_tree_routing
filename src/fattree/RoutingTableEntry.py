# Class to represent the routing table LFT entry

#the LUT routing table entries contain typically the following fields:
#destination LID
#port to reach destination LID
#number of hops
#if the route is optimal

#such structure can be retrieved with the following steps:
#running sudo ibdiagnet --routing
#reading file located by default under /var/tmp/ibdiagnet/ibdiagnet2.fdbs for unicast

#this Python class does not contain optimal field - it is not needed
#instead it contains additional auxiliary fields for debugging
# and to a bit more easily retreive information on the network graph
class RoutingTableEntry():
    def __init__(self, destinationGuid=-1, destinationLid=-1, destinationType="Hca"
                 , nextHopGuid=-1, nextHopLid=-1, localHopPort=-1,
                 localHopPortNo=-1, remoteHopPortNo=-1, isForPlaceKeeper=False, keeperSwitchGuid=-1):

        self.destinationGuid=destinationGuid
        self.destinationLid=destinationLid
        self.destinationType=destinationType
        self.nextHopGuid=nextHopGuid
        self.nextHopLid=nextHopLid
        self.localHopPort=localHopPort
        self.localHopPortNo=localHopPortNo
        self.remoteHopPortNo = remoteHopPortNo
        #To indicate that the LFT represents routing for the place keeper
        self.isForPlaceKeeper=isForPlaceKeeper
        self.keeperSwitchGuid=keeperSwitchGuid