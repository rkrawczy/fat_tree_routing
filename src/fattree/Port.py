# Class to represent the Port
# It is used by the Node class and its derivatives
# Note: For an HCA, each Port has a different GUID/LID
# Note: For a Switch, each Port (and a switch) have THE same GUID/LID
class Port():
    LID = -1
    portGuid = 0
    def __init__(self, portNo, portGuid=-1, neighborGuid=-1, neighborPortNo=0):
        self.portGuid=portGuid
        self.portLid=-1
        #could be port GUID or node GUID, depending on the device
        self.neighborGuid=neighborGuid
        self.neighborPortNo = neighborPortNo
        self.portNo=portNo

