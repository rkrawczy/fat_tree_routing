from fattree.Node import *

#Class to represent a port place keeper in the node
class PlaceKeeper(Node):
    def __init__(self, ports=[], dummyLid=-1, dummyGuid=-1, parentGuid=0):
        Node.__init__(self,ports,maxPorts=1)
        self.dummyLid=dummyLid
        self.dummyGuid=dummyGuid
        self.parentGuid=parentGuid