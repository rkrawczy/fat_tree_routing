from fattree.NodeMeta import *
from fattree.Switch import *
from fattree.RoutingTable import *

from fattree.RoutingTableEntry import *
from fattree.PortMetaGroup import *
from fattree.PlaceKeeper import *
from fattree.IO import *

# Class with main routing algorithms methods
class FatTreeAlg:
    #Public API methods

    #Do the fat tree routing algorithm in the application
    #Given the network topology, list of ROOT switches
    #And an option to print out data & progress
    #Input:
    # Network topology array with Node objects
    # Spine switches GUID list obtained with read_roots_from_file
    #Outputs LFTs and metada for further linear shift test
    @staticmethod
    def generate_lfts_from_routing(network, spineSwitchesGuidList=[], beVerbose=False):
        if beVerbose==True :
          print("******do_fat_tree_routing-STARTING ALGORITHM")
          print("******do_fat_tree_routing-CONSTRUCTING METADATA")
        all_meta, max_hcas_per_switch=FatTreeAlg.__generate_meta(network, spineSwitchesGuidList)
        FatTreeAlg.__set_place_keepers(all_meta,max_hcas_per_switch)
        if beVerbose :
          print("max hcas per switch",max_hcas_per_switch )
          print("******do_fat_tree_routing-PRINTING METADATA")
          FatTreeAlg.__print_meta(all_meta)

        #Generating empty routing table used later for the algolgorithm
        routing_tables=[]
        for node_meta in all_meta :
            if type(node_meta.node).__name__=="Switch":
                Table = RoutingTable(node_meta.node.nodeGuid)
                routing_tables.append(Table)

        if beVerbose==True :
          print("******do_fat_tree_routing-DOING ROUTING. Can take a few seconds.")
        FatTreeAlg.__routing_algorithm(network, all_meta, routing_tables)
        return routing_tables, all_meta

    #Read
    #Input:
    # Network topology array with Node objects
    # Spine switches GUID list obtained with read_roots_from_file
    # Lft file name
    #option to be more verbose
    #Outputs LFTs and metada for further linear shift test
    @staticmethod
    def get_lfts_from_file(network, lftFile, spineSwitchesGuidList=[], beVerbose=False):

        if beVerbose :
          print("******do_fat_tree_routing-STARTING ALGORITHM")
          print("******do_fat_tree_routing-GENERATINF METADATA")
        all_meta, max_hcas_per_switch=FatTreeAlg.__generate_meta(network, spineSwitchesGuidList)
        FatTreeAlg.__set_place_keepers(all_meta,max_hcas_per_switch)
        if beVerbose :
          print("******do_fat_tree_routing- FOUND max number of HCAs per switch: ",max_hcas_per_switch )
          print("******do_fat_tree_routing-PRINTING METADATA")
          FatTreeAlg.__print_meta(all_meta)

        routing_tables = IO.read_tables_from_LFT_file(lftFile)
        FatTreeAlg.__update_metadata_from_file_read_lfts(routing_tables, all_meta)

        return routing_tables, all_meta

    #Read shift pattern from the OpenSM-like file
    #Input:
    # Metadata as a parameter to update them
    # shift_file name
    @staticmethod
    def get_shift_pattern_from_file(shiftFile, networkMetadata):
        shift_list = IO.read_shift_pattern_order_from_file(shiftFile)
        shift_nodes = FatTreeAlg.__assign_shift_list_to_real_nodes(shift_list, networkMetadata)

        return shift_nodes

    #Produce the shift pattern with the dummy nodes internally in the application
    #Based on the network metadata input parameter
    @staticmethod
    def generate_shift_pattern_from_network(networkMetadata):
        hcas_n_keepers_list = []
        for meta in networkMetadata:
            if type(meta.node).__name__ == "Switch":
                for port in meta.node.ports:
                    neighbor_meta = FatTreeAlg.__find_node_meta_by_GUID(port.neighborGuid, networkMetadata)
                    if type(neighbor_meta.node).__name__ == "Hca":
                        hcas_n_keepers_list.append(neighbor_meta.node)
                for keeper in meta.placeKeepers:
                    hcas_n_keepers_list.append("DUMMY")
                    # hcas_n_keepers_list.append(keeper)
        return hcas_n_keepers_list


    #Read network LIDs from the OpenSM guid2lid file
    #Input:
    # network topology as a param to update its LIDs,
    # lid_file name
    @staticmethod
    def get_network_LIDs_from_file(network, lidFile):
        guid_2_lid_table= IO.read_guid_2_lid_table_from_guid2lid(lidFile).get_all_entries()

        for entry in guid_2_lid_table:
            GUID = entry[0]
            LID = entry[1]
            for node in network:
                if type(node).__name__ == "Switch":
                    if node.nodeGuid == GUID:
                        node.nodeLid=LID
                        break
                if type(node).__name__ == "Hca":
                    port = node.get_port_by_guid(GUID)
                    if port !=[]:
                        port.portLid=LID
                        break


    #Assign LIDs internally in the application
    #Input:
    # network topology as a param to update its LIDs,
    @staticmethod
    def set_network_LIDs(network):
        next_lid=1
        for node in network:
            if type(node).__name__=="Switch" :
                node.nodeLid=next_lid
                for port in node.ports:
                    port.portLid=next_lid
                next_lid+=1
            if type(node).__name__ == "Hca":
                for port in node.ports :
                    port.portLid=next_lid
                    next_lid+=1

    #Print routing tables to the console
    #printKeepers - display information about the dummy nodes
    @staticmethod
    def print_routing_tables(routingTables, printKeepers=False):
        for switch_table in routingTables:
            print("\nPRINTING LFT's for switch GUID: ", switch_table.switchGuid)
            for entry in switch_table.routingTableEntries:
                if entry.isForPlaceKeeper==False :
                    print("HOST SWITCH GUID: ", switch_table.switchGuid, "DEST GUID: ",
                          entry.destinationGuid, "DEST LID: ", entry.destinationLid,
                        "NEXT HOP GUID:", entry.nextHopGuid, "NEXT HOP LID: ", entry.nextHopLid,
                        "LOCAL PORT HOP NO:", entry.localHopPortNo, "REMOTE PORT HOP NO:", entry.remoteHopPortNo)
                elif printKeepers==True :
                    print("HOST SWITCH GUID: ", switch_table.switchGuid, "DEST (KEEPER DUMMY) GUID: ",
                          entry.destinationGuid, "DEST LID: ", entry.destinationLid,
                        "NEXT HOP GUID:", entry.nextHopGuid, "NEXT HOP LID: ", entry.nextHopLid,
                        "LOCAL PORT HOP NO:", entry.localHopPortNo, "REMOTE PORT HOP NO:", entry.remoteHopPortNo,
                          "SWITCH GUID OF KEEPER: ", entry.keeperSwitchGuid)


    # find neighbor node of a port in metadata
    # Used by Traffic Checker
    @staticmethod
    def find_node_by_GUID(searchedGUID, network):
        # check switches first
        for node in network:
            if type(node).__name__ == "Switch" and node.nodeGuid == searchedGUID:
                return node
        for node in network:
            if type(node).__name__ == "Hca":
                for port in node.ports:
                    if port.portGuid == searchedGUID:
                        return node
        #No such node found
        return None

    #Private Implementation-related methods

    # Generate network metadata from the network and the list of root switches
    @staticmethod
    def __generate_meta(network, spineSwitchesGuidList=[]):
        network_metadata=[]
        for node in network :
            isSwitch=(type(node).__name__=="Switch")
            isSpine=False
            level=-1
            if isSwitch and node.nodeGuid in spineSwitchesGuidList:
                #Found spine switch
                isSpine=True
                level=0

            metaEntry=NodeMeta(node,isSpine=isSpine,level=level)
            network_metadata.append(metaEntry)
        #spines switches are found and level seto to 0 at this point
        #now set the other nodes
        FatTreeAlg.__set_nodes_levels(network_metadata)
        FatTreeAlg.__set_neighbors_ports_levels(network_metadata)
        max_hcas_per_switch=FatTreeAlg.__get_max_hcas_per_switch(network_metadata)

        return network_metadata, max_hcas_per_switch

    # assign levels to nodes
    # For the LHCb use case
    # Level 0 are the roots
    # 1 Level are the leaf switches
    # 2 Level are the HCAs
    @staticmethod
    def __set_nodes_levels(networkMetadata):
        for meta_node in networkMetadata :
            if meta_node.isSpine :
                level_1=1
                #Iterating through switches
                for neighbor in meta_node.node.ports :
                    if type(meta_node.node).__name__ != "Switch":
                        print ("ERROR. 1st level device should always be the switch")
                    #Setting switch Level 1
                    neighbor_meta_node=FatTreeAlg.__find_node_meta_by_GUID(neighbor.neighborGuid, networkMetadata)
                    neighbor_meta_node.level=level_1
                #Now go for the 2nd level nodes which should be HCAs
                #Iterating through HCAs
                for neighbor in meta_node.node.ports :
                    neighbor_meta_node=FatTreeAlg.__find_node_meta_by_GUID(neighbor.neighborGuid, networkMetadata)
                    for second_neighbor in neighbor_meta_node.node.ports :
                        second_neighbor_meta_node=FatTreeAlg.__find_node_meta_by_GUID(second_neighbor.neighborGuid, networkMetadata)
                        if type(second_neighbor_meta_node.node).__name__=="Hca" and second_neighbor_meta_node.level==(-1):
                            #Setting HCAs with level 2
                            second_neighbor_meta_node.level=2

    #Update the levels of the neighbors
    #This is to know if links are going up or down the tree
    @staticmethod
    def __set_neighbors_ports_levels(networkMetadata):
        for meta_node in networkMetadata :
            for port_meta in meta_node.portsMetaList :
                n_guid = port_meta.port.neighborGuid
                n_meta=FatTreeAlg.__find_node_meta_by_GUID(n_guid, networkMetadata)
                n_level = n_meta.level
                port_meta.neighborLevel = n_level


    # find neighbor node of a port in metadata
    @staticmethod
    def __find_node_meta_by_GUID(searchedGUID, networkMetadata):
        # check switches first
        for meta_node in networkMetadata :
            if type(meta_node.node).__name__=="Switch" and meta_node.node.nodeGuid == searchedGUID :
                return meta_node
        for meta_node in networkMetadata :
            if type(meta_node.node).__name__ == "Hca" :
                for port in meta_node.node.ports :
                    if port.portGuid == searchedGUID :
                        return meta_node
        #No such node found
        return None



    #print metadata
    @staticmethod
    def __print_meta(networkMetadata):
        for index,meta_node in enumerate(networkMetadata):
            print("->Node metadata entry no", index, " Type: ", meta_node.type, " Is spine: ", meta_node.isSpine, " Level: ", meta_node.level )

            if type(meta_node.node).__name__=="Switch" :
                print("It is a switch with LID: ", meta_node.node.nodeLid, " Number of place keepers : ", len(meta_node.placeKeepers), " : ")
                for keeper in meta_node.placeKeepers:
                    print ("keeper entry for Switch GUID: ", keeper.parentGuid,
                           " dummy GUID: ", keeper.dummyGuid,
                           " dummy LID: ", keeper.dummyLid)

            if type(meta_node.node).__name__=="Hca" :
                for port in meta_node.node.ports :
                    print("It is an HCA. Port LID: ", port.portLid)
            print ("\n")




    # the routing algorithm
    #Calls __assign_up_going_port_by_descending and __assign_down_going_port_by_ascending
    #This algorithm follows the steps of an implementation of the Mellanox algorithm
    #Documented in Wiley paper
    #Input:
    # network topology
    # Network metadata
    @staticmethod
    def __routing_algorithm(network, networkMetadata, routingTables):
        #foreach leaf switch
        for meta_entry in networkMetadata:
            if type(meta_entry.node).__name__=="Switch" and meta_entry.level==1 :
                #print("found a leaf switch")
                #foreach compute node
                for switch_port in meta_entry.node.ports:
                    # If neighbor is an HCA
                    hca_meta=FatTreeAlg.__find_node_meta_by_GUID(switch_port.neighborGuid, networkMetadata)
                    if type(hca_meta.node).__name__=="Hca":
                        # Obtain the LID of the compute node
                        hca_port = hca_meta.node.get_port_by_guid(switch_port.neighborGuid)
                        hca_lid=hca_port.portLid
                        hca_guid=hca_port.portGuid
                        #print("Found an HCA with a port LID: ",hca_lid, "and GID: ", "hca_gid",hca_guid)
                        # Set local LFT(LID) of the port connecting to compute node

                        # (Get switch connected to the HCA)

                        lft_entry = RoutingTableEntry(destinationGuid=hca_guid, destinationLid=hca_lid,
                                                      nextHopGuid=hca_guid, nextHopLid=hca_lid,
                                                      localHopPort=switch_port, localHopPortNo=switch_port.portNo, remoteHopPortNo=hca_port.portNo)
                        lft = RoutingTable.find_switch_lft_by_guid(routingTables, meta_entry.node.nodeGuid)
                        #print("appending table entry in main algorithm for node GID: ",meta_entry.node.NodeGUID, " next hop GUID: ",
                        #      hca_guid, "DEST GUID: ", hca_guid,
                        #      " local port hop: ", lft_entry.local_hop_port_no)
                        lft.routingTableEntries.append(lft_entry)
                        meta_port = meta_entry.find_meta_by_port(switch_port)
                        meta_port.usedForMeta=True
                        #call assign-down-going-port-by-ascending
                        if meta_entry.isSpine == False:
                            #print("*********main_algorithm-CALLING assign_down_going_port_by_ascending")
                            FatTreeAlg.__assign_down_going_port_by_ascending(network, networkMetadata, routingTables, hca_guid, hca_lid, meta_entry)
                    else:
                        continue
                #now do the same thing for the place keepers
                for keeper in meta_entry.placeKeepers:
                    # (Get switch connected to the HCA)
                    lft_entry = RoutingTableEntry(destinationGuid=keeper.dummyGuid, destinationLid=keeper.dummyLid,
                                                  nextHopGuid=keeper.dummyGuid, nextHopLid=keeper.dummyLid,
                                                  localHopPort=-1, localHopPortNo=-1, remoteHopPortNo=-1,
                                                  isForPlaceKeeper=True, keeperSwitchGuid=meta_entry.node.nodeGuid)
                    lft = RoutingTable.find_switch_lft_by_guid(routingTables, meta_entry.node.nodeGuid)
                    #print("appending table entry in main algorithm for KEEPER for GID: ",
                    #      meta_entry.node.NodeGUID,
                    #      " next hop GUID: ", keeper.DummyGUID,
                    #      " DEST GUID: ", keeper.DummyGUID,
                    #      " local port hop: ", lft_entry.local_hop_port_no)
                    lft.routingTableEntries.append(lft_entry)
                    #meta_port = meta_entry.find_meta_by_port(switch_port)
                    #meta_port.usedForMeta=True
                    ##call assign-down-going-port-by-ascending
                    if meta_entry.isSpine == False:
                        #print("*********main_algorithm-CALLING assign_down_going_port_by_ascending")
                        FatTreeAlg.__assign_down_going_port_by_ascending(network, networkMetadata, routingTables,
                                                                         keeper.dummyGuid, keeper.dummyLid, meta_entry, True)
                    pass

        pass

    #The assign_up_going_port_by_descending method implementation
    #The method is a direct implementation of the Mellanox fat tree algorithm
    @staticmethod
    def __assign_up_going_port_by_descending(network, networkMetadata,
                                             routingTables, destinationGuid,
                                             destinationLid, switchMeta,
                                             is_place_keeper=False):
        #find groups
        downlink_port_meta_groups = FatTreeAlg.__get_downlink_port_meta_groups(networkMetadata, switchMeta)
        #foreach down-going-port-group
        for meta_group in downlink_port_meta_groups:

            #skip this group if the LFT(LID) port is part of this group
            assigned_port, assigned_port_no = FatTreeAlg.__get_lft_lid_port(routingTables, switchMeta, destinationGuid, destinationLid)
            if assigned_port_no != -1:
                link_in_group=False
                for port_meta in meta_group.portsMetas:
                    if assigned_port is port_meta.port:
                        link_in_group=True
                if link_in_group==True:
                    #Port of this group already in the LID, skipping"
                    continue

            #skip this group if the remote node is a compute node
            if len(meta_group.portsMetas) == 1 :
                neighbor_guid = meta_group.portsMetas[0].port.neighborGuid
                neighbor_meta = FatTreeAlg.__find_node_meta_by_GUID(neighbor_guid, networkMetadata)
                if type(neighbor_meta.node).__name__=="Hca" :
                    #Neighbor is an HCA, skipping the group
                    continue

            #find the least loaded port in the group (scan in indexing order)

            min_load_port_meta = PortMetaGroup.find_least_loaded_port_meta(meta_group)

            #r-port is the remote port connected to it
            r_switch_meta=FatTreeAlg.__find_node_meta_by_GUID(min_load_port_meta.port.neighborGuid, networkMetadata)
            r_port_no=min_load_port_meta.port.neighborPortNo
            r_port_meta=PortMeta.get_meta_by_port_number(r_port_no,r_switch_meta)

            #assign the remote switch node LFT(LID) to r-port
            if(is_place_keeper==True):
                switch_keeper_GUID=FatTreeAlg.__find_keeper_switch(networkMetadata, destinationGuid)
            else :
                switch_keeper_GUID = -1

            lft_entry = RoutingTableEntry(destinationGuid=destinationGuid, destinationLid=destinationLid,
                                          nextHopGuid=switchMeta.node.nodeGuid, nextHopLid=switchMeta.node.nodeLid,
                                          localHopPort=r_port_meta.port, localHopPortNo=r_port_no,
                                          remoteHopPortNo=min_load_port_meta.port.portNo,
                                          isForPlaceKeeper=is_place_keeper,
                                          keeperSwitchGuid=switch_keeper_GUID)
            lft = RoutingTable.find_switch_lft_by_guid(routingTables, r_switch_meta.node.nodeGuid)
            lft.routingTableEntries.append(lft_entry)

            #increase r-port usage counter
            r_port_meta.usedForUp+=1
            min_load_port_meta.usedForUp+=1
            FatTreeAlg.__assign_up_going_port_by_descending(network, networkMetadata,
                                                            routingTables, destinationGuid, destinationLid, r_switch_meta, is_place_keeper)

    #The assign_down_going_port_by_ascending method implementation
    #The method is a direct implementation of the Mellanox fat tree algorithm
    @staticmethod
    def __assign_down_going_port_by_ascending(network, networkMetadata,
                                              routingTables, destinationGuid,
                                              destinationLid, switchMeta,
                                              isPlaceKeeper=False):
        #print("IN assign_down_going_port_by1_ascending")
        #print("IN assign_down_going_port_by_ascending with destination Hca GUID ", destinationGuid)

        #hca_destination = FatTreeAlg.find_node_meta_by_GUID(destinationGuid, networkMetadata)
        #Given: a switch and an LID
        #In this method a switch metadata and an LID GUID are used

        #Find a least loaded port of all the groups
        #this practically means find any up-link that is free
        #with additional stop dondition for a spine for all up-going routines
        if switchMeta.isSpine == False:
            uplink_port_meta, uplink_node_meta = FatTreeAlg.__find_least_used_remote_downlink_port(networkMetadata, switchMeta)
            #(now get the remote_port on the remote node)
            neighbor_port_no = uplink_port_meta.port.neighborPortNo
            remote_node_link_port_meta=PortMeta.get_meta_by_port_number(neighbor_port_no,uplink_node_meta)
            #assign LFT(LID) of the remote switch  to that port
            #TODO
            #uplink_node_meta
            if(isPlaceKeeper==True):
                switch_keeper_GUID=FatTreeAlg.__find_keeper_switch(networkMetadata, destinationGuid)
            else :
                switch_keeper_GUID = -1

            lft_entry = RoutingTableEntry(destinationGuid=destinationGuid, destinationLid=destinationLid,
                                          nextHopGuid=switchMeta.node.nodeGuid, nextHopLid=switchMeta.node.nodeLid,
                                          localHopPort=remote_node_link_port_meta.port,
                                          localHopPortNo=remote_node_link_port_meta.port.portNo,
                                          remoteHopPortNo=uplink_port_meta.port.portNo,
                                          isForPlaceKeeper=isPlaceKeeper,
                                          keeperSwitchGuid=switch_keeper_GUID)
            #uplink_port_meta
            lft = RoutingTable.find_switch_lft_by_guid(routingTables, uplink_node_meta.node.nodeGuid)
            lft.routingTableEntries.append(lft_entry)

            #track that port usage
            #uplink_port_meta.usedForMeta=True
            uplink_port_meta.usedForDown+=1
            # assign_down_going_port_by_ascending on remote switch
            FatTreeAlg.__assign_down_going_port_by_ascending(network, networkMetadata,
                                                             routingTables, destinationGuid,
                                                             destinationLid, uplink_node_meta,
                                                             isPlaceKeeper)
        #else :
        # SPINE SWITCH, NO FURTHER GOING UP NEEDED
        #assign_up_going_port_by_descending on current switch
        FatTreeAlg.__assign_up_going_port_by_descending(network, networkMetadata,
                                                        routingTables, destinationGuid,
                                                        destinationLid, switchMeta,
                                                        isPlaceKeeper)
    #Method used for links assignment
    #Find least used link
    # used to then assign it the routing path
    @staticmethod
    def __find_least_used_remote_downlink_port(networkMetadata, switchMeta):
        least_used_counter=-1
        least_used_port_meta=-1
        least_used_neighbor_meta=-1
        for port_meta in switchMeta.portsMetaList :
            #in unused ports find neighbor and check if it is a switch
            neighbor_GUID=port_meta.port.neighborGuid
            neighbor_meta=FatTreeAlg.__find_node_meta_by_GUID(neighbor_GUID, networkMetadata)
            if(type(neighbor_meta.node).__name__=="Switch"):
                #print("LEaf switch found unused port to the spine switch")
                #sanity check if the neighbor has a lowe level
                #should not happen with only 2 levels of switches
                if(switchMeta.level<=neighbor_meta.level):
                    print("ERROR. LEVELS MISMATCH. THIS SHOULD NOT HAPPEN")
                if least_used_counter == -1 or least_used_counter > port_meta.usedForDown:
                    least_used_counter=port_meta.usedForDown
                    least_used_port_meta=port_meta
                    least_used_neighbor_meta=neighbor_meta
        return least_used_port_meta, least_used_neighbor_meta


    # get port and its number that has to be used in the switch
    # when trying to reach destination HCA defined by destinationHcaGuid
    def __get_lft_lid_port(routingTables, switchMeta, destinationHcaGuid, destinationHcaLid):
        switch_lft = RoutingTable.find_switch_lft_by_guid(routingTables, switchMeta.node.nodeGuid)
        for entry in switch_lft.routingTableEntries:
            if entry.destinationGuid == destinationHcaGuid:
                return entry.localHopPort, entry.localHopPortNo
        return -1,-1

    #Method to retreive the groups with down links for a given switch
    @staticmethod
    def __get_downlink_port_meta_groups(networkMetadata, switchMeta):
        down_link_ports=[]
        switch_level=switchMeta.level
        port_meta_groups=FatTreeAlg.__get_port_meta_groups(switchMeta)
        for group in port_meta_groups:
            if switch_level < group.get_n_check_ports_level():
                down_link_ports.append(group)
        return down_link_ports


    #Method used for retreiving port groups for a given switch
    @staticmethod
    def __get_port_meta_groups(switchMeta):
        port_meta_groups=[]
        #first make groups by getting all neighbors GUIDs
        for port_meta in switchMeta.portsMetaList :
            #check if the neighbor switch ID is in the list
            group = PortMetaGroup.find_by_guid(port_meta_groups, port_meta.port.neighborGuid)
            if(group ==[]):
                port_meta_groups.append(PortMetaGroup(port_meta.port.neighborGuid))
                group = port_meta_groups[-1]
            group.portsMetas.append(port_meta)
        return port_meta_groups

    #Get maximal number of HCAs for the switches
    #Used then to know how many place keepers we must assign
    #For the switches that have less than that
    @staticmethod
    def __get_max_hcas_per_switch(switchMetadata):
        max_hcas_per_switch=0
        for meta_node in switchMetadata:
            if type(meta_node.node).__name__=="Switch":
                local_hcas_no=0
                #found switch in meta nodes
                for port in meta_node.node.ports:

                    neighbor_node_meta=FatTreeAlg.__find_node_meta_by_GUID(port.neighborGuid, switchMetadata)
                    if type(neighbor_node_meta.node).__name__=="Hca":
                        local_hcas_no+=1
                if local_hcas_no > max_hcas_per_switch:
                    max_hcas_per_switch=local_hcas_no

        return max_hcas_per_switch

    #Assign place keepers to the nodes that have less than maximal number of HCAs
    def __set_place_keepers(allMeta, maxHcasPerSwitch):

        # putting node dummies in the topology
        #This is to implement the dealing with uneven
        #population of switch with nodes
        next_place_keeper_guid=-1
        next_place_keeper_lid=-1

        for meta_node in allMeta:
            if type(meta_node.node).__name__=="Switch":
                local_hcas_no=0
                #print("found switch in meta nodes. GUID:", meta_node.node.NodeGUID)
                for port in meta_node.node.ports:
                    neighbor_node_meta=FatTreeAlg.__find_node_meta_by_GUID(port.neighborGuid, allMeta)
                    if type(neighbor_node_meta.node).__name__=="Hca":
                        local_hcas_no+=1
                if local_hcas_no < maxHcasPerSwitch:
                    if meta_node.isSpine==True :
                        #Spine switch. No HCAs place keepers needed. Ignoring.
                        continue
                    else :
                    #add place keepers
                        how_many_place_keepers = maxHcasPerSwitch - local_hcas_no
                        for i in range(how_many_place_keepers) :
                            place_keeper=PlaceKeeper(dummyLid=next_place_keeper_lid,
                                                     dummyGuid=next_place_keeper_guid,
                                                     parentGuid=meta_node.node.nodeGuid)
                            meta_node.placeKeepers.append(place_keeper)
                            next_place_keeper_guid-=1
                            next_place_keeper_lid-=1

    #Find the switch holding a keeper with a destinationGuid
    @staticmethod
    def __find_keeper_switch(networkMeta, destinationGuid):
        for meta_node in networkMeta:
            if type(meta_node.node).__name__=="Switch" :
                for keeper in meta_node.placeKeepers :
                    if(keeper.dummyGuid==destinationGuid) :
                        return meta_node.node.nodeGuid
        return -1

    #generate & update metadata for LFTs read from a file
    @staticmethod
    def __update_metadata_from_file_read_lfts(routingTables, allMeta):
        for meta_entry in allMeta:
            #print("FOUND " + meta_entry.type)
            if meta_entry.type == "Switch":

                lft = RoutingTable.find_switch_lft_by_guid(routingTables, meta_entry.node.nodeGuid)

                for entry in lft.routingTableEntries:
                    FatTreeAlg.__update_incomplete_lft_entry(entry, meta_entry, allMeta)

        pass

    #update LIDs in the network given the guid2lid mapping
    @staticmethod
    def __update_lids_in_network(guidToLidTable, network):
        #Filling missing fields in metadata
        for node_entry in network:
            if type(node_entry).__name__ == "Switch":
                #found a switch
                lid=guidToLidTable.get_lid_by_guid(node_entry.nodeGuid)
                node_entry.nodeLid=lid
                print("SWITCH GUID in hex: " + hex(node_entry.nodeGuid) + " new LID: " + str(node_entry.nodeLid) + " IN HEX: " + hex(node_entry.nodeLid))
            if type(node_entry).__name__ == "Hca":
                #found an Hca
                for port in node_entry.ports:
                    hca_opensm_guid = node_entry.transform_port_guid_topo_to_opensm(port.portNo)
                    lid=guidToLidTable.get_lid_by_guid(hca_opensm_guid)
                    port.portLid=lid
                    print("Hca port (OPENSM) GUID in hex: " + hex(hca_opensm_guid) + " new LID: " + str(port.portLid) + " IN HEX: " + hex(port.portLid))
            pass
        pass

    # supplement informatio for the LFT read from a file given the metadata
    @staticmethod
    def __update_incomplete_lft_entry(lftEntry, parentMeta, allMeta):

        meta_dest, dest_guid = NodeMeta.find_node_meta_by_lid(lftEntry.destinationLid, allMeta)
        if meta_dest==-1 and dest_guid==-1:
            #ltf entry to non-existent node. skipping.
            return
        local_port_no = lftEntry.localHopPortNo
        neighbor_guid = parentMeta.node.get_port_by_num(local_port_no).neighborGuid
        neighbor_port_no = parentMeta.node.get_port_by_num(local_port_no).neighborPortNo
        

        neighbor_meta = NodeMeta.find_node_meta_by_guid(neighbor_guid, allMeta)

        neighbor_port = neighbor_meta.node.get_port_by_num(neighbor_port_no)
        neighbor_port_lid = neighbor_port.portLid

        #neighbor_port_lid = parent_meta.node.ports[local_port_no].PortLID
        #neighbor_port = lft_entry.next_hop_LID = parent_meta.node.ports[local_port_no]

        lftEntry.destinationGuid= dest_guid
        lftEntry.nextHopGuid = neighbor_guid
        lftEntry.nextHopLid = neighbor_port_lid
        lftEntry.remote_hop_port = neighbor_port
        lftEntry.remoteHopPortNo = neighbor_port_no

    #Associate shift LiDs with the existing metadata
    @staticmethod
    def __assign_shift_list_to_real_nodes(shiftLidsList, networkMetadata):
        shift_nodes = []

        #last known switch
        #this is to assign dummy to existing place keeper
        #in a given switch
        #this code relies on the fact that keepers are assigned at the end of the code
        for lid_entry in shiftLidsList:
            #print(entry)
            if lid_entry == int(0xFFFF):
                keepers_switch_meta = NodeMeta.find_node_meta_by_guid(last_used_switch_guid, networkMetadata)

                shift_nodes.append(keepers_switch_meta.placeKeepers[used_keepers])
                used_keepers+=1
                #shift_nodes.append("DUMMY")
                pass
            else:
                entry_meta, entry_GUID = NodeMeta.find_node_meta_by_lid(lid_entry, networkMetadata)
                shift_nodes.append(entry_meta.node)
                #produce_shift_pattern_from_network
                last_used_switch_guid= entry_meta.node.get_port_by_guid(entry_GUID).neighborGuid
                used_keepers=0
        return shift_nodes


    #TODO - produce shift for the subset of nodes
    @staticmethod
    def produce_shift_pattern_tuples_subset(shiftOrder, nodesList, ibnodes):

        ibnodes_reduced=FatTreeAlg.__reduce_ibnodes_by_list(nodesList, ibnodes)
        subset=FatTreeAlg.__associate_nodes_with_pattern(shiftOrder, ibnodes_reduced)
        return subset

    # TODO - produce shift for the subset of nodes
    @staticmethod
    def __reduce_ibnodes_by_list(nodes_list,ibnodes):
        ibnodes_reduced =[]
        for node in nodes_list:
            for ibnode in ibnodes:
                if node[0]==ibnode[0]:
                    ibnodes_reduced.append(ibnode)
        return ibnodes_reduced

    # TODO - produce shift for the subset of nodes
    @staticmethod
    def __associate_nodes_with_pattern(shiftOrder, ibnodesReduced):
        shift_final =[]
        for node in shiftOrder:
            was_node=False
            for ibnode in ibnodesReduced:
                #print("COMPARING "+ hex(node.ports[0].PortGUID) + " and " + hex(ibnode[2]) +"\n")
                if node.ports[0].portGuid == ibnode[2]:
                    shift_final.append((ibnode[0], ibnode[1]))
                    was_node=True
            if was_node == False:
                shift_final.append(("dummy",-1))
        return shift_final




