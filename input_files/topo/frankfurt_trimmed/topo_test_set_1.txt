#
# Topology file: generated on Tue Dec 13 14:26:25 2016
#
# Initiated from node e41d2d0300b92fa0 port e41d2d0300b92fa1

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003005845fc
caguid=0x7cfe9003005845fc
Ca	1 "H-7cfe9003005845fc"		# "node002 HCA-1"
[1](7cfe9003005845fc) 	"S-e41d2d0300bfaf40"[19]		# lid 47 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003005843f0
caguid=0x7cfe9003005843f0
Ca	1 "H-7cfe9003005843f0"		# "node030 HCA-1"
[1](7cfe9003005843f0) 	"S-e41d2d0300b13660"[24]		# lid 64 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0xe41d2d030067b7c8
caguid=0xe41d2d030067b7c8
Ca	1 "H-e41d2d030067b7c8"		# "node034 HCA-1"
[1](e41d2d030067b7c8) 	"S-e41d2d0300b13660"[23]		# lid 59 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0xe41d2d030067b7dc
caguid=0xe41d2d030067b7dc
Ca	1 "H-e41d2d030067b7dc"		# "node035 HCA-1"
[1](e41d2d030067b7dc) 	"S-e41d2d0300b13660"[22]		# lid 61 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0xe41d2d0300675c66
caguid=0xe41d2d0300675c66
Ca	1 "H-e41d2d0300675c66"		# "node036 HCA-1"
[1](e41d2d0300675c66) 	"S-e41d2d0300b13660"[21]		# lid 23 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300584608
caguid=0x7cfe900300584608
Ca	1 "H-7cfe900300584608"		# "node029 HCA-1"
[1](7cfe900300584608) 	"S-e41d2d0300b13660"[20]		# lid 9 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0xe41d2d030062113e
caguid=0xe41d2d030062113e
Ca	1 "H-e41d2d030062113e"		# "node033 HCA-1"
[1](e41d2d030062113e) 	"S-e41d2d0300b13660"[19]		# lid 15 lmc 0 "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003005845c4
caguid=0x7cfe9003005845c4
Ca	1 "H-7cfe9003005845c4"		# "node007 HCA-1"
[1](7cfe9003005845c4) 	"S-e41d2d0300bfaf40"[24]		# lid 40 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003005842b4
caguid=0x7cfe9003005842b4
Ca	1 "H-7cfe9003005842b4"		# "node004 HCA-1"
[1](7cfe9003005842b4) 	"S-e41d2d0300bfaf40"[22]		# lid 38 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300584588
caguid=0x7cfe900300584588
Ca	1 "H-7cfe900300584588"		# "node006 HCA-1"
[1](7cfe900300584588) 	"S-e41d2d0300bfaf40"[23]		# lid 31 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe90030058459c
caguid=0x7cfe90030058459c
Ca	1 "H-7cfe90030058459c"		# "node001 HCA-1"
[1](7cfe90030058459c) 	"S-e41d2d0300bfaf40"[21]		# lid 74 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR


vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300584600
caguid=0x7cfe900300584600
Ca	1 "H-7cfe900300584600"		# "node003 HCA-1"
[1](7cfe900300584600) 	"S-e41d2d0300bfaf40"[20]		# lid 35 lmc 0 "SwitchIB Mellanox Technologies" lid 10 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe90030026885c
caguid=0x7cfe90030026885c
Ca	1 "H-7cfe90030026885c"		# "node075 HCA-1"
[1](7cfe90030026885c) 	"S-7cfe900300198b00"[6]		# lid 121 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003002687f8
caguid=0x7cfe9003002687f8
Ca	1 "H-7cfe9003002687f8"		# "node079 HCA-1"
[1](7cfe9003002687f8) 	"S-7cfe900300198b00"[5]		# lid 119 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300268a38
caguid=0x7cfe900300268a38
Ca	1 "H-7cfe900300268a38"		# "node072 HCA-1"
[1](7cfe900300268a38) 	"S-7cfe900300198b00"[4]		# lid 123 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300268810
caguid=0x7cfe900300268810
Ca	1 "H-7cfe900300268810"		# "node076 HCA-1"
[1](7cfe900300268810) 	"S-7cfe900300198b00"[3]		# lid 114 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe9003002687fc
caguid=0x7cfe9003002687fc
Ca	1 "H-7cfe9003002687fc"		# "node080 HCA-1"
[1](7cfe9003002687fc) 	"S-7cfe900300198b00"[2]		# lid 118 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0x1013
sysimgguid=0x7cfe900300268998
caguid=0x7cfe900300268998
Ca	1 "H-7cfe900300268998"		# "node071 HCA-1"
[1](7cfe900300268998) 	"S-7cfe900300198b00"[1]		# lid 122 lmc 0 "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0xe41d2d0300b13660
switchguid=0xe41d2d0300b13660(e41d2d0300b13660)
Switch	36 "S-e41d2d0300b13660"		# "MF0;rack1-switch2:MSB7700/U1" enhanced port 0 lid 91 lmc 0
[1]	"S-e41d2d0300bfaf60"[28]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[2]	"S-e41d2d0300bfaf60"[25]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[3]	"S-e41d2d0300bfaf60"[30]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[4]	"S-e41d2d0300bfaf60"[29]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[5]	"S-e41d2d0300bfaf60"[26]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[6]	"S-e41d2d0300bfaf60"[27]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[7]	"S-e41d2d0300bfb980"[28]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[8]	"S-e41d2d0300bfb980"[30]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[9]	"S-e41d2d0300bfb980"[25]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[10]	"S-e41d2d0300bfb980"[27]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[11]	"S-e41d2d0300bfb980"[26]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[12]	"S-e41d2d0300bfb980"[29]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[13]	"S-e41d2d0300bfb960"[26]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[14]	"S-e41d2d0300bfb960"[29]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[15]	"S-e41d2d0300bfb960"[27]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[16]	"S-e41d2d0300bfb960"[30]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[17]	"S-e41d2d0300bfb960"[25]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[18]	"S-e41d2d0300bfb960"[28]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[19]	"H-e41d2d030062113e"[1](e41d2d030062113e) 		# "node033 HCA-1" lid 15 4xEDR
[20]	"H-7cfe900300584608"[1](7cfe900300584608) 		# "node029 HCA-1" lid 9 4xEDR
[21]	"H-e41d2d0300675c66"[1](e41d2d0300675c66) 		# "node036 HCA-1" lid 23 4xEDR
[22]	"H-e41d2d030067b7dc"[1](e41d2d030067b7dc) 		# "node035 HCA-1" lid 61 4xEDR
[23]	"H-e41d2d030067b7c8"[1](e41d2d030067b7c8) 		# "node034 HCA-1" lid 59 4xEDR
[24]	"H-7cfe9003005843f0"[1](7cfe9003005843f0) 		# "node030 HCA-1" lid 64 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0xe41d2d0300bfaf40
switchguid=0xe41d2d0300bfaf40(e41d2d0300bfaf40)
Switch	36 "S-e41d2d0300bfaf40"		# "SwitchIB Mellanox Technologies" base port 0 lid 10 lmc 0
[1]	"S-e41d2d0300bfaf60"[20]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[2]	"S-e41d2d0300bfaf60"[23]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[3]	"S-e41d2d0300bfaf60"[22]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[4]	"S-e41d2d0300bfaf60"[19]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[5]	"S-e41d2d0300bfaf60"[21]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[6]	"S-e41d2d0300bfaf60"[24]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[7]	"S-e41d2d0300bfb980"[20]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[8]	"S-e41d2d0300bfb980"[21]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[9]	"S-e41d2d0300bfb980"[22]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[10]	"S-e41d2d0300bfb980"[24]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[11]	"S-e41d2d0300bfb980"[19]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[12]	"S-e41d2d0300bfb980"[23]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[13]	"S-e41d2d0300bfb960"[24]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[14]	"S-e41d2d0300bfb960"[21]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[15]	"S-e41d2d0300bfb960"[23]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[16]	"S-e41d2d0300bfb960"[22]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[17]	"S-e41d2d0300bfb960"[19]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[18]	"S-e41d2d0300bfb960"[20]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[19]	"H-7cfe9003005845fc"[1](7cfe9003005845fc) 		# "node002 HCA-1" lid 47 4xEDR
[20]	"H-7cfe900300584600"[1](7cfe900300584600) 		# "node003 HCA-1" lid 35 4xEDR
[21]	"H-7cfe90030058459c"[1](7cfe90030058459c) 		# "node001 HCA-1" lid 74 4xEDR
[22]	"H-7cfe9003005842b4"[1](7cfe9003005842b4) 		# "node004 HCA-1" lid 38 4xEDR
[23]	"H-7cfe900300584588"[1](7cfe900300584588) 		# "node006 HCA-1" lid 31 4xEDR
[24]	"H-7cfe9003005845c4"[1](7cfe9003005845c4) 		# "node007 HCA-1" lid 40 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0x7cfe900300198b00
switchguid=0x7cfe900300198b00(7cfe900300198b00)
Switch	36 "S-7cfe900300198b00"		# "MF0;rack2-switch1:MSB7700/U1" enhanced port 0 lid 45 lmc 0
[1]	"H-7cfe900300268998"[1](7cfe900300268998) 		# "node071 HCA-1" lid 122 4xEDR
[2]	"H-7cfe9003002687fc"[1](7cfe9003002687fc) 		# "node080 HCA-1" lid 118 4xEDR
[3]	"H-7cfe900300268810"[1](7cfe900300268810) 		# "node076 HCA-1" lid 114 4xEDR
[4]	"H-7cfe900300268a38"[1](7cfe900300268a38) 		# "node072 HCA-1" lid 123 4xEDR
[5]	"H-7cfe9003002687f8"[1](7cfe9003002687f8) 		# "node079 HCA-1" lid 119 4xEDR
[6]	"H-7cfe90030026885c"[1](7cfe90030026885c) 		# "node075 HCA-1" lid 121 4xEDR
[19]	"S-e41d2d0300bfb960"[10]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[20]	"S-e41d2d0300bfb960"[11]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[21]	"S-e41d2d0300bfb960"[12]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[22]	"S-e41d2d0300bfb960"[9]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[23]	"S-e41d2d0300bfb960"[7]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[24]	"S-e41d2d0300bfb960"[8]		# "SwitchIB Mellanox Technologies" lid 7 4xEDR
[25]	"S-e41d2d0300bfaf60"[12]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[26]	"S-e41d2d0300bfaf60"[11]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[27]	"S-e41d2d0300bfaf60"[9]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[28]	"S-e41d2d0300bfaf60"[7]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[29]	"S-e41d2d0300bfaf60"[8]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[30]	"S-e41d2d0300bfaf60"[10]		# "SwitchIB Mellanox Technologies" lid 4 4xEDR
[31]	"S-e41d2d0300bfb980"[10]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[32]	"S-e41d2d0300bfb980"[12]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[33]	"S-e41d2d0300bfb980"[8]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[34]	"S-e41d2d0300bfb980"[7]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[35]	"S-e41d2d0300bfb980"[9]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR
[36]	"S-e41d2d0300bfb980"[11]		# "SwitchIB Mellanox Technologies" lid 5 4xEDR


vendid=0x2c9
devid=0xcb20
sysimgguid=0xe41d2d0300bfb960
switchguid=0xe41d2d0300bfb960(e41d2d0300bfb960)
Switch	36 "S-e41d2d0300bfb960"		# "SwitchIB Mellanox Technologies" base port 0 lid 7 lmc 0
[7]	"S-7cfe900300198b00"[23]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[8]	"S-7cfe900300198b00"[24]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[9]	"S-7cfe900300198b00"[22]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[10]	"S-7cfe900300198b00"[19]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[11]	"S-7cfe900300198b00"[20]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[12]	"S-7cfe900300198b00"[21]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[19]	"S-e41d2d0300bfaf40"[17]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[20]	"S-e41d2d0300bfaf40"[18]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[21]	"S-e41d2d0300bfaf40"[14]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[22]	"S-e41d2d0300bfaf40"[16]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[23]	"S-e41d2d0300bfaf40"[15]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[24]	"S-e41d2d0300bfaf40"[13]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[25]	"S-e41d2d0300b13660"[17]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[26]	"S-e41d2d0300b13660"[13]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[27]	"S-e41d2d0300b13660"[15]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[28]	"S-e41d2d0300b13660"[18]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[29]	"S-e41d2d0300b13660"[14]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[30]	"S-e41d2d0300b13660"[16]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0xe41d2d0300bfb980
switchguid=0xe41d2d0300bfb980(e41d2d0300bfb980)
Switch	36 "S-e41d2d0300bfb980"		# "SwitchIB Mellanox Technologies" base port 0 lid 5 lmc 0
[7]	"S-7cfe900300198b00"[34]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[8]	"S-7cfe900300198b00"[33]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[9]	"S-7cfe900300198b00"[35]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[10]	"S-7cfe900300198b00"[31]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[11]	"S-7cfe900300198b00"[36]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[12]	"S-7cfe900300198b00"[32]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[19]	"S-e41d2d0300bfaf40"[11]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[20]	"S-e41d2d0300bfaf40"[7]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[21]	"S-e41d2d0300bfaf40"[8]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[22]	"S-e41d2d0300bfaf40"[9]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[23]	"S-e41d2d0300bfaf40"[12]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[24]	"S-e41d2d0300bfaf40"[10]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[25]	"S-e41d2d0300b13660"[9]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[26]	"S-e41d2d0300b13660"[11]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[27]	"S-e41d2d0300b13660"[10]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[28]	"S-e41d2d0300b13660"[7]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[29]	"S-e41d2d0300b13660"[12]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[30]	"S-e41d2d0300b13660"[8]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0xe41d2d0300bfaf60
switchguid=0xe41d2d0300bfaf60(e41d2d0300bfaf60)
Switch	36 "S-e41d2d0300bfaf60"		# "SwitchIB Mellanox Technologies" base port 0 lid 4 lmc 0
[7]	"S-7cfe900300198b00"[28]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[8]	"S-7cfe900300198b00"[29]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[9]	"S-7cfe900300198b00"[27]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[10]	"S-7cfe900300198b00"[30]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[11]	"S-7cfe900300198b00"[26]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[12]	"S-7cfe900300198b00"[25]		# "MF0;rack2-switch1:MSB7700/U1" lid 45 4xEDR
[19]	"S-e41d2d0300bfaf40"[4]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[20]	"S-e41d2d0300bfaf40"[1]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[21]	"S-e41d2d0300bfaf40"[5]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[22]	"S-e41d2d0300bfaf40"[3]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[23]	"S-e41d2d0300bfaf40"[2]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[24]	"S-e41d2d0300bfaf40"[6]		# "SwitchIB Mellanox Technologies" lid 10 4xEDR
[25]	"S-e41d2d0300b13660"[2]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[26]	"S-e41d2d0300b13660"[5]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[27]	"S-e41d2d0300b13660"[6]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[28]	"S-e41d2d0300b13660"[1]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[29]	"S-e41d2d0300b13660"[4]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR
[30]	"S-e41d2d0300b13660"[3]		# "MF0;rack1-switch2:MSB7700/U1" lid 91 4xEDR


